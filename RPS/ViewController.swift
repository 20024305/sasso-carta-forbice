//
//  ViewController.swift
//  RPS
//
//  Created by User on 17/01/2020.
//  Copyright © 2020 User. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateUI(.start)
    }
    
    func updateUI(_ state: GameState) {
        gameStatus.text = state.result
        switch state {
        case .win:
            view.backgroundColor = UIColor.green
        case .lose:
            view.backgroundColor = UIColor.red
        case .draw:
            view.backgroundColor = UIColor.gray
        case .start:
            appIcon.text = "🤖"
            playAgainButton.isHidden = true
            playAgainButton.isEnabled = false
            rockButton.isHidden = false
            paperButton.isHidden = false
            scissorsButton.isHidden = false
            rockButton.isEnabled = true
            paperButton.isEnabled = true
            scissorsButton.isEnabled = true
            
            view.backgroundColor = UIColor.white
        }
    }
    
    func play(_ playerSign: Sign) {
        let computerSign = randomSign()
        updateUI(playerSign.result(opponent: computerSign))
        appIcon.text = computerSign.emoji
        rockButton.isEnabled = false
        paperButton.isEnabled = false
        scissorsButton.isEnabled = false
        
        rockButton.isHidden = (playerSign != .rock)
        paperButton.isHidden = (playerSign != .paper)
        scissorsButton.isHidden = (playerSign != .scissors)
        
        playAgainButton.isHidden = false
        playAgainButton.isEnabled = true
    }
    
    @IBOutlet weak var appIcon: UILabel!
    @IBOutlet weak var gameStatus: UILabel!
    @IBOutlet weak var rockButton: UIButton!
    @IBOutlet weak var paperButton: UIButton!
    @IBOutlet weak var scissorsButton: UIButton!
    @IBOutlet weak var playAgainButton: UIButton!
    @IBAction func rockAction(_ sender: UIButton) {
        play(.rock)
    }
    
    @IBAction func paperAction(_ sender: UIButton) {
        play(.paper)
    }
    
    @IBAction func scissorsAction(_ sender: UIButton) {
        play(.scissors)
    }
    
    @IBAction func playAgainAction(_ sender: UIButton) {
        updateUI(.start)
    }
}

