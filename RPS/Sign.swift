//
//  Sign.swift
//  RPS
//
//  Created by User on 17/01/2020.
//  Copyright © 2020 User. All rights reserved.
import GameplayKit
//

import Foundation

let randomChoice = GKRandomDistribution(lowestValue: 0, highestValue: 2)


enum Sign {
    case rock, paper, scissors
    
    var emoji: String {
        switch self {
        case .rock:
            return "👊🏻"
        case .paper:
            return "🖐🏻"
        case .scissors:
            return "✌🏻"
        }
    }
    
    func result(opponent :Sign) -> GameState{
        if opponent == self {
            return GameState.draw
        }
        if opponent == .paper && self == .rock {
            return GameState.lose
        }
        if opponent == .scissors && self == .paper {
            return GameState.lose
        }
        if opponent == .rock && self == .scissors {
            return GameState.lose
        }
        return GameState.win
        
    }

}


func randomSign() -> Sign {
    let sign = randomChoice.nextInt()
    if sign == 0 {
        return .rock
    } else if sign == 1 {
        return .paper
    } else {
        return .scissors
    }
}
