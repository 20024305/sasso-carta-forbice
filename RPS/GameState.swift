//
//  GameState.swift
//  RPS
//
//  Created by User on 17/01/2020.
//  Copyright © 2020 User. All rights reserved.
//

import Foundation

enum GameState {
    case start, lose, win, draw
    var result: String
    {
        switch self {
        case .start:
            return "Start"
        case .lose:
            return "Lose"
        case .win:
            return "Win"
        case .draw:
            return "Draw"
        }
    }
}
